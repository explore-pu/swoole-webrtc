#!/usr/bin/env node

process.title = 'mediasoup-server';
process.env.DEBUG = process.env.DEBUG || '*INFO* *WARN* *ERROR*';

const config = require('./config');

console.log('config.js:\n', JSON.stringify(config, null, '  '));

const fs = require('fs');
const https = require('https');
const url = require('url');
const protoo = require('protoo-server');
const mediasoup = require('mediasoup');
const express = require('express');
const bodyParser = require('body-parser');
const { AwaitQueue } = require('awaitqueue');
const utils = require('./lib/utils');
const Room = require('./lib/Room');

// 用于管理聊天室的异步队列
// @type {AwaitQueue}
const queue = new AwaitQueue();

// 按 roomId 编制索引的 Room 实例映射
// @type {Map<Number, Room>}
const rooms = new Map();

// HTTPS 服务器
// @type {https.Server}
let httpsServer;

// Express 应用
// @type {Function}
let expressApp;

// Protoo WebSocket 服务器
// @type {protoo.WebSocketServer}
let protooWebSocketServer;

// mediasoup 进程
// @type {Array<mediasoup.Worker>}
const mediasoupWorkers = [];

// 要使用的下一个 mediasoup Worker 的索引
// @type {Number}
let nextMediasoupWorkerIdx = 0;

(async () => {
  try {
    // 运行 mediasoup 进程
    await runMediasoupWorkers();

    // 创建 Express 应用程序
    await createExpressApp();

    // 运行 HTTPS 服务器
    await runHttpsServer();

    // 运行 protoo WebSocketServer
    await runProtooWebSocketServer();

    // 每 X 秒记录一次房间状态
    setInterval(() => {
      for (const room of rooms.values()) {
        room.logStatus();
      }
    }, 120000);
  } catch (err) {
    console.error(err);
  }
})();

/**
 * 启动配置文件中给出的任意数量的 mediasoup 进程
 */
async function runMediasoupWorkers() {
  const {numWorkers} = config.mediasoup;

  console.info(`运行了 ${numWorkers} 个mediasoup进程\n`);

  for (let i = 0; i < numWorkers; ++i) {
    const worker = await mediasoup.createWorker(
      {
        dtlsCertificateFile: config.mediasoup.workerSettings.dtlsCertificateFile,
        dtlsPrivateKeyFile: config.mediasoup.workerSettings.dtlsPrivateKeyFile,
        logLevel: config.mediasoup.workerSettings.logLevel,
        logTags: config.mediasoup.workerSettings.logTags,
        rtcMinPort: Number(config.mediasoup.workerSettings.rtcMinPort),
        rtcMaxPort: Number(config.mediasoup.workerSettings.rtcMaxPort)
      });

    worker.on('died', () => {
      console.error('mediasoup进程挂了，2秒后退出......\n', JSON.stringify({pid: worker.pid}));

      setTimeout(() => process.exit(1), 2000);
    });

    mediasoupWorkers.push(worker);

    // 在此 Worker 中创建一个 WebRtcServer
    if (process.env.MEDIASOUP_USE_WEBRTC_SERVER !== 'false') {
      // 每个 mediasoup 进程将运行自己的 WebRtcServer，因此它们不能共享相同的侦听端口。因此，我们增加了每个 Worker 的 config.js 中的值
      const webRtcServerOptions = utils.clone(config.mediasoup.webRtcServerOptions);
      const portIncrement = mediasoupWorkers.length - 1;

      for (const listenInfo of webRtcServerOptions.listenInfos) {
        listenInfo.port += portIncrement;
      }

      const webRtcServer = await worker.createWebRtcServer(webRtcServerOptions);

      worker.appData.webRtcServer = webRtcServer;
    }

    // 每 X 秒记录一次辅助角色资源使用情况
    setInterval(async () => {
      const usage = await worker.getResourceUsage();

      console.info('mediasoup进程资源使用情况', `${JSON.stringify({pid: worker.pid, usage: usage})}\n`);
    }, 120000);
  }
}

/**
 * 创建基于 Express 的 API 服务器来管理 Broadcaster 请求
 */
async function createExpressApp() {
  console.info('正在创建 Express 应用程序...\n');

  expressApp = express();

  expressApp.use(bodyParser.json());

  /**
   * 对于每个 API 请求，请验证路径中的 roomId 是否与现有 room 匹配
   */
  expressApp.param('roomId', (req, res, next, roomId) => {
    queue.push(async () => {
      req.room = await getOrCreateRoom({roomId, consumerReplicas: 0});

      next();
    }).catch((error) => {
      console.error(`通过 Broadcaster 创建房间或加入房间失败 ${error}\n`);

      next(error);
    });
  });

  /**
   * API GET 资源，用于返回房间的 mediasoup 路由器 RTP 功能
   */
  expressApp.get('/rooms/:roomId', (req, res) => {
    const data = req.room.getRouterRtpCapabilities();

    res.status(200).json(data);
  });

  /**
   * POST API 创建 Broadcaster
   */
  expressApp.post('/rooms/:roomId/broadcasters', async (req, res, next) => {
    const {
      id,
      displayName,
      device,
      rtpCapabilities
    } = req.body;

    try {
      const data = await req.room.createBroadcaster({
        id,
        displayName,
        device,
        rtpCapabilities
      });

      res.status(200).json(data);
    } catch (error) {
      next(error);
    }
  });

  /**
   * DELETE API 删除 Broadcaster
   */
  expressApp.delete('/rooms/:roomId/broadcasters/:broadcasterId', (req, res) => {
    const {broadcasterId} = req.params;

    req.room.deleteBroadcaster({broadcasterId});

    res.status(200).send('broadcaster deleted');
  });

  /**
   * POST API 创建与 Broadcaster 关联的 mediasoup 传输。
   * 它可以是 PlainTransport 或 WebRtcTransport，具体取决于正文中的类型参数
   * PlainTransport 还有其他参数
   */
  expressApp.post('/rooms/:roomId/broadcasters/:broadcasterId/transports', async (req, res, next) => {
    const {broadcasterId} = req.params;
    const {type, rtcpMux, comedia, sctpCapabilities} = req.body;

    try {
      const data = await req.room.createBroadcasterTransport({
        broadcasterId,
        type,
        rtcpMux,
        comedia,
        sctpCapabilities
      });

      res.status(200).json(data);
    } catch (error) {
      next(error);
    }
  });

  /**
   * POST API，用于连接属于广播公司的传输
   * 如果 PlainTransport 是在将 comedia 选项设置为 true 的情况下创建的，则不需要它
   */
  expressApp.post('/rooms/:roomId/broadcasters/:broadcasterId/transports/:transportId/connect', async (req, res, next) => {
    const {broadcasterId, transportId} = req.params;
    const {dtlsParameters} = req.body;

    try {
      const data = await req.room.connectBroadcasterTransport({
        broadcasterId,
        transportId,
        dtlsParameters
      });

      res.status(200).json(data);
    } catch (error) {
      next(error);
    }
  });

  /**
   * POST API 创建与 Broadcaster 关联的 mediasoup Producer
   * 必须在其中创建生产者的确切传输在 URL 路径中发出信号
   * Body 参数包括 Producer 的 kind 和 rtpParameters
   */
  expressApp.post('/rooms/:roomId/broadcasters/:broadcasterId/transports/:transportId/producers', async (req, res, next) => {
    const {broadcasterId, transportId} = req.params;
    const {kind, rtpParameters} = req.body;

    try {
      const data = await req.room.createBroadcasterProducer({
        broadcasterId,
        transportId,
        kind,
        rtpParameters
      });

      res.status(200).json(data);
    } catch (error) {
      next(error);
    }
  });

  /**
   * POST API 创建与 Broadcaster 关联的 mediasoup Consumer
   * 必须在 URL 路径中创建 Consumer 的确切传输
   * 查询参数必须包含要使用的所需 producerId
   */
  expressApp.post('/rooms/:roomId/broadcasters/:broadcasterId/transports/:transportId/consume', async (req, res, next) => {
    const {broadcasterId, transportId} = req.params;
    const {producerId} = req.query;

    try {
      const data = await req.room.createBroadcasterConsumer({
        broadcasterId,
        transportId,
        producerId
      });

      res.status(200).json(data);
    } catch (error) {
      next(error);
    }
  });

  /**
   * POST API 创建与 Broadcaster 关联的 mediasoup DataConsumer
   * 必须在 URL 路径中发出 DataConsumer 的确切传输信号
   * 查询正文必须包含要使用的所需 producerId
   */
  expressApp.post('/rooms/:roomId/broadcasters/:broadcasterId/transports/:transportId/consume/data', async (req, res, next) => {
    const {broadcasterId, transportId} = req.params;
    const {dataProducerId} = req.body;

    try {
      const data = await req.room.createBroadcasterDataConsumer({
        broadcasterId,
        transportId,
        dataProducerId
      });

      res.status(200).json(data);
    } catch (error) {
      next(error);
    }
  });

  /**
   * POST API 创建与 Broadcaster 关联的 mediasoup DataProducer
   * 必须在其中创建 DataProducer 的确切传输信号在
   */
  expressApp.post('/rooms/:roomId/broadcasters/:broadcasterId/transports/:transportId/produce/data', async (req, res, next) => {
    const {broadcasterId, transportId} = req.params;
    const {label, protocol, sctpStreamParameters, appData} = req.body;

    try {
      const data = await req.room.createBroadcasterDataProducer({
        broadcasterId,
        transportId,
        label,
        protocol,
        sctpStreamParameters,
        appData
      });

      res.status(200).json(data);
    } catch (error) {
      next(error);
    }
  });

  /**
   * 错误处理程序
   */
  expressApp.use((error, req, res, next) => {
    if (error) {
      console.warn(`Express 应用程序${error}\n`);

      error.status = error.status || (error.name === 'TypeError' ? 400 : 500);

      res.statusMessage = error.message;
      res.status(error.status).send(String(error));
    } else {
      next();
    }
  });
}

/**
 * 创建 Node.js HTTPS 服务器。它侦听配置文件中给出的 IP 和端口
 * 并重用 Express 应用程序作为请求侦听器
 */
async function runHttpsServer() {
  console.info('正在运行 HTTPS 服务器...\n');

  // HTTPS server for the protoo WebSocket server.
  const tls = {
    cert: fs.readFileSync(config.https.tls.cert),
    key: fs.readFileSync(config.https.tls.key)
  };

  httpsServer = https.createServer(tls, expressApp);

  await new Promise((resolve) => {
    httpsServer.listen(Number(config.https.listenPort), config.https.listenIp, resolve);
  });
}

/**
 * 创建一个 protoo WebSocketServer 以允许来自浏览器的 WebSocket 连接
 */
async function runProtooWebSocketServer() {
  console.info('正在运行 protoo WebSocketServer...\n');

  // 创建 protoo WebSocket 服务器
  protooWebSocketServer = new protoo.WebSocketServer(httpsServer, {
    maxReceivedFrameSize: 960000, // 960 KBytes.
    maxReceivedMessageSize: 960000,
    fragmentOutgoingMessages: true,
    fragmentationThreshold: 960000
  });

  // 处理来自客户端的连接
  protooWebSocketServer.on('connectionrequest', (info, accept, reject) => {
    // 客户端在 URL 查询中指示 roomId 和 peerId
    const u = url.parse(info.request.url, true);
    const roomId = u.query['roomId'];
    const peerId = u.query['peerId'];

    if (!roomId || !peerId) {
      reject(400, 'Connection request without roomId and/or peerId');

      return;
    }

    let consumerReplicas = Number(u.query['consumerReplicas']);

    if (isNaN(consumerReplicas)) {
      consumerReplicas = 0;
    }

    console.info('Protoo 连接请求', JSON.stringify({
      roomId: roomId,
      peerId: peerId,
      address: info.socket.remoteAddress,
      origin: info.origin
    }) + '\n');

    // Serialize this code into the queue to avoid that two peers connecting at
    // the same time with the same roomId create two separate rooms with same
    // roomId.
    queue.push(async () => {
      const room = await getOrCreateRoom({roomId, consumerReplicas});

      // Accept the protoo WebSocket connection.
      const protooWebSocketTransport = accept();

      room.handleProtooConnection({peerId, protooWebSocketTransport});
    }).catch((error) => {
      console.error(`创建聊天室或加入聊天室失败 ${error}\n`);

      reject(error);
    });
  });
}

/**
 * 获取下一个 mediasoup 进程
 */
function getMediasoupWorker() {
  const worker = mediasoupWorkers[nextMediasoupWorkerIdx];

  if (++nextMediasoupWorkerIdx === mediasoupWorkers.length)
    nextMediasoupWorkerIdx = 0;

  return worker;
}

/**
 * 获取 Room 实例（如果不存在，则创建一个）
 */
async function getOrCreateRoom({roomId, consumerReplicas}) {
  let room = rooms.get(roomId);

  // 如果房间不存在，请创建一个新房间
  if (!room) {
    console.info('创建新房间', JSON.stringify({roomId: roomId}) + '\n');

    const mediasoupWorker = getMediasoupWorker();

    room = await Room.create({mediasoupWorker, roomId, consumerReplicas});

    rooms.set(roomId, room);
    room.on('close', () => rooms.delete(roomId));
  }

  return room;
}
