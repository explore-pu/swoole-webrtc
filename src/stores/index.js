import {ref} from 'vue'
import {defineStore} from 'pinia'

export const useStore = defineStore('store', () => {
  const room = ref({
    url: null,
    state: 'new', // new/connecting/connected/disconnected/closed,
    mediasoupClientHandler: undefined,
    activeSpeakerId: null,
    statsPeerId: null,
    faceDetection: false
  });
  const me = ref({
    id: null,
    displayName: null,
    displayNameSet: false,
    device: null,
    canSendMic: false,
    canSendWebcam: false,
    canChangeWebcam: false,
    webcamInProgress: false,
    shareInProgress: false,
    audioOnly: false,
    audioOnlyInProgress: false,
    audioMuted: false,
    restartIceInProgress: false
  });
  const producers = ref({});
  const dataProducers = ref({});
  const peers = ref({});
  const consumers = ref({});
  const dataConsumers = ref({});
  const notifications = ref({});

  //// room ////
  const setRoomState = (state) => {
    room.value.state = state;
  }

  const setRoomMediasoupClientHandler = (handlerName) => {
    room.value.mediasoupClientHandler = handlerName;
  };

  const setRoomStatsPeerId = (peerId) => {
    room.value.statsPeerId = peerId;
  }

  //// me ////
  const setMe = ({ peerId, displayName, displayNameSet, device }) => {
    me.value.id = peerId;
    me.value.displayName = displayName;
    me.value.displayNameSet = displayNameSet;
    me.value.device = device;
  }
  const setMediaCapabilities = (mediaCapabilities) => {
    me.value.canSendMic = mediaCapabilities.canSendMic;
    me.value.canSendWebcam = mediaCapabilities.canSendWebcam;
  }

  const setWebcamInProgress = (flag) => {
    me.value.webcamInProgress = flag;
  }

  const setCanChangeWebcam = (flag) => {
    me.value.canChangeWebcam = flag;
  }

  //// producers ////
  const addProducer = (producer) => {
    producers.value[producer.id] = producer;
  }
  const removeProducer = (producerId) => {
    delete producers.value[producerId];
  }

  //// peers ////
  const addPeer = (peer) => {
    peers.value[peer.id] = peer;
  }

  //// dataConsumers ////
  const addDataConsumer = (dataConsumer, peerId) => {
    const peer = peers[peerId];

    if (!peer) {
      throw new Error('未找到新 DataConsumer 的对等方');
    }

    peers.value[peerId].dataConsumers = [...peer.dataConsumers, dataConsumer.id];

    dataConsumers[dataConsumer.id] = dataConsumer;
  }

  return {
    room,
    me,
    producers,
    dataProducers,
    peers,
    consumers,
    dataConsumers,

    //// room ////
    setRoomState,
    setRoomMediasoupClientHandler,
    setRoomStatsPeerId,

    //// me ////
    setMe,
    setMediaCapabilities,
    setWebcamInProgress,
    setCanChangeWebcam,

    //// producers ////
    addProducer,
    removeProducer,

    //// peers ////
    addPeer,

    //// dataConsumers ////
    addDataConsumer,
  }
});
