import protooClient from 'protoo-client';
import * as mediasoupClient from 'mediasoup-client';

import {useStore} from "@/stores";

const store = useStore();

const externalVideo = false;
const EXTERNAL_VIDEO_SRC = '/resources/videos/video-audio-stereo.mp4';
const VIDEO_CONSTRAINS = {
  qvga: {width: {ideal: 320}, height: {ideal: 240}},
  vga: {width: {ideal: 640}, height: {ideal: 480}},
  hd: {width: {ideal: 1280}, height: {ideal: 720}}
};

export default class RoomClient {
  constructor(roomId, peerId, displayName, device) {
    this._facingMode = 'user'; // 移动端摄像头，前置：'user', 后置：'environment'

    // Closed flag.
    // @type {Boolean}
    this._closed = false;

    // Display name.
    // @type {String}
    this._displayName = displayName;

    // Device info.
    // @type {Object}
    this._device = device;

    // Custom mediasoup-client handler name (to override default browser
    // detection if desired).
    // @type {String}
    this._handlerName = null;

    // Whether we want to force RTC over TCP.
    // @type {Boolean}
    this._forceTcp = false;

    // Whether we want to produce audio/video.
    // @type {Boolean}
    this._produce = true;

    // Whether we should consume.
    // @type {Boolean}
    this._consume = true;

    // Whether we want DataChannels.
    // @type {Boolean}
    this._useDataChannel = true;

    // Force VP8 codec for sending.
    // @type {Boolean}
    this._forceVP8 = false;

    // Force H264 codec for sending.
    // @type {Boolean}
    this._forceH264 = false;

    // Force VP9 codec for sending.
    // @type {Boolean}
    this._forceVP9 = false;

    // Whether simulcast or SVC should be used for webcam.
    // @type {Boolean}
    this._enableWebcamLayers = true;

    // Whether simulcast or SVC should be used in desktop sharing.
    // @type {Boolean}
    this._enableSharingLayers = true;

    // Scalability mode for webcam.
    // @type {String}
    this._webcamScalabilityMode = null;

    // Scalability mode for sharing.
    // @type {String}
    this._sharingScalabilityMode = null;

    // Number of simuclast streams for webcam and sharing.
    // @type {Number}
    this._numSimulcastStreams = 3;

    // External video.
    // @type {HTMLVideoElement}
    this._externalVideo = null;

    // Enabled end-to-end encryption.
    this._e2eKey = null;

    // MediaStream of the external video.
    // @type {MediaStream}
    this._externalVideoStream = null;

    // Next expected dataChannel test number.
    // @type {Number}
    this._nextDataChannelTestNumber = 0;

    // 播放指定视频
    if (externalVideo) {
      this._externalVideo = document.createElement('video');
      this._externalVideo.controls = true;
      this._externalVideo.muted = true;
      this._externalVideo.loop = true;
      this._externalVideo.playsinline = true;
      this._externalVideo.src = EXTERNAL_VIDEO_SRC;
      this._externalVideo.play().catch((error) => console.warn('外部视频播放失败:', error));
    }

    // Protoo URL.
    // @type {String}
    this._protooUrl = `${import.meta.env.APP_WS}:${import.meta.env.APP_WS_PORT}?roomId=${roomId}&peerId=${peerId}&consumerReplicas=null`;

    // protoo-client Peer instance.
    // @type {protooClient.Peer}
    this._protoo = null;

    // mediasoup-client Device instance.
    // @type {mediasoupClient.Device}
    this._mediasoupDevice = null;

    // mediasoup Transport for sending.
    // @type {mediasoupClient.Transport}
    this._sendTransport = null;

    // mediasoup Transport for receiving.
    // @type {mediasoupClient.Transport}
    this._recvTransport = null;

    // Local mic mediasoup Producer.
    // @type {mediasoupClient.Producer}
    this._micProducer = null;

    // Local webcam mediasoup Producer.
    // @type {mediasoupClient.Producer}
    this._webcamProducer = null;

    // Local share mediasoup Producer.
    // @type {mediasoupClient.Producer}
    this._shareProducer = null;

    // Local chat DataProducer.
    // @type {mediasoupClient.DataProducer}
    this._chatDataProducer = null;

    // Local bot DataProducer.
    // @type {mediasoupClient.DataProducer}
    this._botDataProducer = null;

    // mediasoup Consumers.
    // @type {Map<String, mediasoupClient.Consumer>}
    this._consumers = new Map();

    // mediasoup DataConsumers.
    // @type {Map<String, mediasoupClient.DataConsumer>}
    this._dataConsumers = new Map();

    // Map of webcam MediaDeviceInfos indexed by deviceId.
    // @type {Map<String, MediaDeviceInfos>}
    this._webcams = new Map();

    // Local Webcam.
    // @type {Object} with:
    // - {MediaDeviceInfo} [device]
    // - {String} [resolution] - 'qvga' / 'vga' / 'hd'.
    this._webcam = {
      device: null,
      resolution: 'hd'
    };

    if (this._e2eKey && e2e.isSupported()) {
      e2e.setCryptoKey('setCryptoKey', this._e2eKey, true);
    }
  }


  close()
  {
    if (this._closed) {
      return;
    }

    this._closed = true;

    console.debug('close()');

    // Close protoo Peer
    this._protoo.close();

    // Close mediasoup Transports.
    if (this._sendTransport) {
      this._sendTransport.close();
    }

    if (this._recvTransport){
      this._recvTransport.close();
    }

    store.setRoomState('closed');
  }

  async join() {
    const protooTransport = new protooClient.WebSocketTransport(this._protooUrl);

    this._protoo = new protooClient.Peer(protooTransport);

    store.setRoomState('connecting');

    this._protoo.on('open', async () => await this._joinRoom());

    this._protoo.on('failed', () => {
      console.error('WebSocket 连接失败');
      store.setRoomState('connecting');
    });

    this._protoo.on('disconnected', () => {
      console.error('WebSocket 已断开连接');

      // Close mediasoup Transports.
      if (this._sendTransport) {
        this._sendTransport.close();
        this._sendTransport = null;
      }

      if (this._recvTransport) {
        this._recvTransport.close();
        this._recvTransport = null;
      }

      store.setRoomState('closed');
    });

    this._protoo.on('close', () => {
      if (this._closed) {
        return;
      }

      this.close();
    });

    this._protoo.on('request', async (request, accept, reject) => {
      console.log('proto "request" event', {method: request.method, data: request.data});

      switch (request.method) {
        case 'newConsumer': {
          if (!this._consume) {
            reject(403, 'I do not want to consume');

            break;
          }

          const {
            peerId,
            producerId,
            id,
            kind,
            rtpParameters,
            type,
            appData,
            producerPaused
          } = request.data;

          try {
            const consumer = await this._recvTransport.consume({
                id,
                producerId,
                kind,
                rtpParameters,
                // NOTE: Force streamId to be same in mic and webcam and different
                // in screen sharing so libwebrtc will just try to sync mic and
                // webcam streams from the same remote peer.
                streamId: `${peerId}-${appData.share ? 'share' : 'mic-webcam'}`,
                appData: {...appData, peerId} // Trick.
              });

            if (this._e2eKey && e2e.isSupported()) {
              e2e.setupReceiverTransform(consumer.rtpReceiver);
            }

            // Store in the map.
            this._consumers.set(consumer.id, consumer);

            consumer.on('transportclose', () => {
              this._consumers.delete(consumer.id);
            });

            const {spatialLayers, temporalLayers} = mediasoupClient.parseScalabilityMode(consumer.rtpParameters.encodings[0].scalabilityMode);

            store.dispatch(stateActions.addConsumer({
                id: consumer.id,
                type: type,
                locallyPaused: false,
                remotelyPaused: producerPaused,
                rtpParameters: consumer.rtpParameters,
                spatialLayers: spatialLayers,
                temporalLayers: temporalLayers,
                preferredSpatialLayer: spatialLayers - 1,
                preferredTemporalLayer: temporalLayers - 1,
                priority: 1,
                codec: consumer.rtpParameters.codecs[0].mimeType.split('/')[1],
                track: consumer.track
              },
              peerId));

            // We are ready. Answer the protoo request so the server will
            // resume this Consumer (which was paused for now if video).
            accept();

            // If audio-only mode is enabled, pause it.
            if (consumer.kind === 'video' && store.me.audioOnly) {
              this._pauseConsumer(consumer);
            }
          } catch (error) {
            console.error('"newConsumer" request failed:%o', error);

            throw error;
          }

          break;
        }

        case 'newDataConsumer': {
          if (!this._consume) {
            reject(403, 'I do not want to data consume');

            break;
          }

          if (!this._useDataChannel) {
            reject(403, 'I do not want DataChannels');

            break;
          }

          const {
            peerId, // NOTE: Null if bot.
            dataProducerId,
            id,
            sctpStreamParameters,
            label,
            protocol,
            appData
          } = request.data;

          try {
            const dataConsumer = await this._recvTransport.consumeData({
              id,
              dataProducerId,
              sctpStreamParameters,
              label,
              protocol,
              appData: {...appData, peerId} // Trick.
            });

            // Store in the map.
            this._dataConsumers.set(dataConsumer.id, dataConsumer);

            dataConsumer.on('transportclose', () => {
              this._dataConsumers.delete(dataConsumer.id);
            });

            dataConsumer.on('open', () => {
              console.debug('DataConsumer "open" event');
            });

            dataConsumer.on('close', () => {
              console.warn('DataConsumer "close" event');

              this._dataConsumers.delete(dataConsumer.id);

              store.dispatch(requestActions.notify(
                {
                  type: 'error',
                  text: 'DataConsumer closed'
                }));
            });

            dataConsumer.on('error', (error) => {
              console.error('DataConsumer "error" event:%o', error);

              store.dispatch(requestActions.notify(
                {
                  type: 'error',
                  text: `DataConsumer error: ${error}`
                }));
            });

            dataConsumer.on('message', (message) => {
              console.debug('DataConsumer "message" event [streamId:%d]', dataConsumer.sctpStreamParameters.streamId);

              if (message instanceof ArrayBuffer) {
                const view = new DataView(message);
                const number = view.getUint32();

                if (number === Math.pow(2, 32) - 1) {
                  console.warn('dataChannelTest finished!');

                  this._nextDataChannelTestNumber = 0;

                  return;
                }

                if (number > this._nextDataChannelTestNumber) {
                  console.warn(
                    'dataChannelTest: %s packets missing',
                    number - this._nextDataChannelTestNumber);
                }

                this._nextDataChannelTestNumber = number + 1;

                return;
              } else if (typeof message !== 'string') {
                console.warn('ignoring DataConsumer "message" (not a string)');

                return;
              }

              switch (dataConsumer.label) {
                case 'chat': {
                  const peers = store.peers;

                  const peersArray = Object.keys(peers).map((_peerId) => {
                    return peers[_peerId];
                  });
                  const sendingPeer = peersArray.find((peer) => {
                    return peer.dataConsumers.includes(dataConsumer.id);
                  });

                  if (!sendingPeer) {
                    console.warn('DataConsumer "message" from unknown peer');

                    break;
                  }

                  // store.dispatch(requestActions.notify(
                  //   {
                  //     title: `${sendingPeer.displayName} says:`,
                  //     text: message,
                  //     timeout: 5000
                  //   }));

                  break;
                }

                case 'bot': {
                  store.dispatch(requestActions.notify(
                    {
                      title: 'Message from Bot:',
                      text: message,
                      timeout: 5000
                    }));

                  break;
                }
              }
            });

            store.addDataConsumer({
                id: dataConsumer.id,
                sctpStreamParameters: dataConsumer.sctpStreamParameters,
                label: dataConsumer.label,
                protocol: dataConsumer.protocol
              },
              peerId);

            // We are ready. Answer the protoo request.
            accept();
          } catch (error) {
            console.error('"newDataConsumer" request failed:%o', error);

            throw error;
          }

          break;
        }
      }
    });

    this._protoo.on('notification', (notification) => {
      console.debug('proto "notification" event [method:%s, data:%o]', notification.method, notification.data);

      switch (notification.method) {
        case 'producerScore': {
          const {producerId, score} = notification.data;

          store.dispatch(
            stateActions.setProducerScore(producerId, score));

          break;
        }

        case 'newPeer': {
          const peer = notification.data;

          store.addPeer({...peer, consumers: [], dataConsumers: []});

          // store.dispatch(requestActions.notify(
          //   {
          //     text: `${peer.displayName} has joined the room`
          //   }));

          break;
        }

        case 'peerClosed': {
          const {peerId} = notification.data;

          store.dispatch(stateActions.removePeer(peerId));

          break;
        }

        // case 'peerDisplayNameChanged': {
        // const {peerId, displayName, oldDisplayName} = notification.data;

        // store.dispatch(stateActions.setPeerDisplayName(displayName, peerId));

        // store.dispatch(requestActions.notify(
        //   {
        //     text: `${oldDisplayName} is now ${displayName}`
        //   }));

        // break;
        // }

        case 'downlinkBwe': {
          console.debug('downlinkBwe event', notification.data);

          break;
        }

        case 'consumerClosed': {
          const {consumerId} = notification.data;
          const consumer = this._consumers.get(consumerId);

          if (!consumer)
            break;

          consumer.close();
          this._consumers.delete(consumerId);

          const {peerId} = consumer.appData;

          store.dispatch(
            stateActions.removeConsumer(consumerId, peerId));

          break;
        }

        case 'consumerPaused': {
          const {consumerId} = notification.data;
          const consumer = this._consumers.get(consumerId);

          if (!consumer)
            break;

          consumer.pause();

          store.dispatch(
            stateActions.setConsumerPaused(consumerId, 'remote'));

          break;
        }

        case 'consumerResumed': {
          const {consumerId} = notification.data;
          const consumer = this._consumers.get(consumerId);

          if (!consumer)
            break;

          consumer.resume();

          store.dispatch(
            stateActions.setConsumerResumed(consumerId, 'remote'));

          break;
        }

        case 'consumerLayersChanged': {
          const {consumerId, spatialLayer, temporalLayer} = notification.data;
          const consumer = this._consumers.get(consumerId);

          if (!consumer)
            break;

          store.dispatch(stateActions.setConsumerCurrentLayers(
            consumerId, spatialLayer, temporalLayer));

          break;
        }

        case 'consumerScore': {
          const {consumerId, score} = notification.data;

          store.dispatch(
            stateActions.setConsumerScore(consumerId, score));

          break;
        }

        case 'dataConsumerClosed': {
          const {dataConsumerId} = notification.data;
          const dataConsumer = this._dataConsumers.get(dataConsumerId);

          if (!dataConsumer)
            break;

          dataConsumer.close();
          this._dataConsumers.delete(dataConsumerId);

          const {peerId} = dataConsumer.appData;

          store.dispatch(
            stateActions.removeDataConsumer(dataConsumerId, peerId));

          break;
        }

        case 'activeSpeaker': {
          const {peerId} = notification.data;

          store.dispatch(
            stateActions.setRoomActiveSpeaker(peerId));

          break;
        }

        default: {
          console.error('unknown protoo notification.method', notification.method);
        }
      }
    });
  }

  /**
   * 启用麦克风
   *
   * @returns {Promise<void>}
   */
  async enableMic() {
    console.info('enableMic()');

    if (this._micProducer) {
      return;
    }

    if (!this._mediasoupDevice.canProduce('audio')) {
      console.error('enableMic() | cannot produce audio');

      return;
    }

    let track;

    try {
      if (!this._externalVideo) {
        console.info('enableMic() | calling getUserMedia()');

        const stream = await navigator.mediaDevices.getUserMedia({ audio: true });

        track = stream.getAudioTracks()[0];
      } else {
        const stream = await this._getExternalVideoStream();

        track = stream.getAudioTracks()[0].clone();
      }

      this._micProducer = await this._sendTransport.produce({
        track,
        codecOptions : {
          opusStereo : true,
          opusDtx    : true,
          opusFec    : true,
          opusNack   : true
        }
        // NOTE: for testing codec selection.
        // codec : this._mediasoupDevice.rtpCapabilities.codecs
        // 	.find((codec) => codec.mimeType.toLowerCase() === 'audio/pcma')
      });

      if (this._e2eKey && e2e.isSupported()) {
        e2e.setupSenderTransform(this._micProducer.rtpSender);
      }

      store.addProducer({
        id: this._micProducer.id,
        paused: this._micProducer.paused,
        track: this._micProducer.track,
        rtpParameters: this._micProducer.rtpParameters,
        codec: this._micProducer.rtpParameters.codecs[0].mimeType.split('/')[1]
      });

      this._micProducer.on('transportclose', () => {
        this._micProducer = null;
      });

      this._micProducer.on('trackended', () => {
        console.error('麦克风已断开连接！');

        this.disableMic().catch(() => {});
      });
    } catch (error) {
      console.error('enableMic() | failed:%o', error);

      // store.dispatch(requestActions.notify(
      //   {
      //     type : 'error',
      //     text : `Error enabling microphone: ${error}`
      //   }));

      if (track) {
        track.stop();
      }
    }
  }

  async disableMic()
  {
    logger.debug('disableMic()');

    if (!this._micProducer)
      return;

    this._micProducer.close();

    store.dispatch(
      stateActions.removeProducer(this._micProducer.id));

    try
    {
      await this._protoo.request(
        'closeProducer', { producerId: this._micProducer.id });
    }
    catch (error)
    {
      store.dispatch(requestActions.notify(
        {
          type : 'error',
          text : `Error closing server-side mic Producer: ${error}`
        }));
    }

    this._micProducer = null;
  }

  async muteMic()
  {
    logger.debug('muteMic()');

    this._micProducer.pause();

    try
    {
      await this._protoo.request(
        'pauseProducer', { producerId: this._micProducer.id });

      store.dispatch(
        stateActions.setProducerPaused(this._micProducer.id));
    }
    catch (error)
    {
      logger.error('muteMic() | failed: %o', error);

      store.dispatch(requestActions.notify(
        {
          type : 'error',
          text : `Error pausing server-side mic Producer: ${error}`
        }));
    }
  }

  async unmuteMic()
  {
    logger.debug('unmuteMic()');

    this._micProducer.resume();

    try
    {
      await this._protoo.request(
        'resumeProducer', { producerId: this._micProducer.id });

      store.dispatch(
        stateActions.setProducerResumed(this._micProducer.id));
    }
    catch (error)
    {
      logger.error('unmuteMic() | failed: %o', error);

      store.dispatch(requestActions.notify(
        {
          type : 'error',
          text : `Error resuming server-side mic Producer: ${error}`
        }));
    }
  }

  /**
   * 启用摄像头
   *
   * @returns {Promise<void>}
   */
  async enableWebcam() {
    console.info('enableWebcam()');

    if (this._webcamProducer) {
      return;
    } else if (this._shareProducer) {
      await this.disableShare();
    }

    if (!this._mediasoupDevice.canProduce('video')) {
      console.error('enableWebcam() | cannot produce video');

      return;
    }

    let track;
    let device;

    store.setWebcamInProgress(true);

    try {
      if (!this._externalVideo)
      {
        await this._updateWebcams();
        device = this._webcam.device;

        const { resolution } = this._webcam;

        if (!device) {
          throw new Error('no webcam devices');
        }

        console.info('enableWebcam() | calling getUserMedia()');

        const stream = await navigator.mediaDevices.getUserMedia({
          video : {
            deviceId : { ideal: device.deviceId },
            ...VIDEO_CONSTRAINS[resolution]
          }
        });

        track = stream.getVideoTracks()[0];
      } else {
        device = { label: 'external video' };

        const stream = await this._getExternalVideoStream();

        track = stream.getVideoTracks()[0].clone();
      }

      let encodings;

      let codec;
      const codecOptions = {videoGoogleStartBitrate : 1000};

      if (this._forceVP8) {
        codec = this._mediasoupDevice.rtpCapabilities.codecs.find((c) => {
          return c.mimeType.toLowerCase() === 'video/vp8';
        });

        if (!codec) {
          throw new Error('desired VP8 codec+configuration is not supported');
        }
      } else if (this._forceH264) {
        codec = this._mediasoupDevice.rtpCapabilities.codecs.find((c) => {
          return c.mimeType.toLowerCase() === 'video/h264';
        });

        if (!codec) {
          throw new Error('desired H264 codec+configuration is not supported');
        }
      } else if (this._forceVP9) {
        codec = this._mediasoupDevice.rtpCapabilities.codecs.find((c) => {
          return c.mimeType.toLowerCase() === 'video/vp9';
        });

        if (!codec) {
          throw new Error('desired VP9 codec+configuration is not supported');
        }
      }

      if (this._enableWebcamLayers) {
        // If VP9 is the only available video codec then use SVC.
        const firstVideoCodec = this._mediasoupDevice.rtpCapabilities.codecs.find((c) => {
          return c.kind === 'video';
        });

        // VP9 with SVC.
        if ((this._forceVP9 && codec) || firstVideoCodec.mimeType.toLowerCase() === 'video/vp9') {
          encodings = [{
            maxBitrate: 5000000,
            scalabilityMode: this._webcamScalabilityMode || 'L3T3_KEY'
          }];
        }
        // VP8 or H264 with simulcast.
        else {
          encodings = [{
            scaleResolutionDownBy: 1,
            maxBitrate: 5000000,
            scalabilityMode: this._webcamScalabilityMode || 'L1T3'
          }];

          if (this._numSimulcastStreams > 1) {
            encodings.unshift({
              scaleResolutionDownBy: 2,
              maxBitrate: 1000000,
              scalabilityMode: this._webcamScalabilityMode || 'L1T3'
            });
          }

          if (this._numSimulcastStreams > 2) {
            encodings.unshift({
              scaleResolutionDownBy: 4,
              maxBitrate: 500000,
              scalabilityMode: this._webcamScalabilityMode || 'L1T3'
            });
          }
        }
      }

      this._webcamProducer = await this._sendTransport.produce({
        track,
        encodings,
        codecOptions,
        codec
      });

      if (this._e2eKey && e2e.isSupported()) {
        e2e.setupSenderTransform(this._webcamProducer.rtpSender);
      }

      store.addProducer({
        id: this._webcamProducer.id,
        deviceLabel: device.label,
        type: this._getWebcamType(device),
        paused: this._webcamProducer.paused,
        track: this._webcamProducer.track,
        rtpParameters: this._webcamProducer.rtpParameters,
        codec: this._webcamProducer.rtpParameters.codecs[0].mimeType.split('/')[1]
      });

      this._webcamProducer.on('transportclose', () => {
        this._webcamProducer = null;
      });

      this._webcamProducer.on('trackended', () => {
        console.error('摄像头已断开连接！');

        this.disableWebcam().catch(() => {});
      });
    } catch (error) {
      console.error('enableWebcam() | failed', error);

      if (track) {
        track.stop();
      }
    }

    store.setWebcamInProgress(false);
  }

  async disableWebcam()
  {
    logger.debug('disableWebcam()');

    if (!this._webcamProducer)
      return;

    this._webcamProducer.close();

    store.dispatch(
      stateActions.removeProducer(this._webcamProducer.id));

    try
    {
      await this._protoo.request(
        'closeProducer', { producerId: this._webcamProducer.id });
    }
    catch (error)
    {
      store.dispatch(requestActions.notify(
        {
          type : 'error',
          text : `Error closing server-side webcam Producer: ${error}`
        }));
    }

    this._webcamProducer = null;
  }

  async changeWebcam()
  {
    logger.debug('changeWebcam()');

    store.dispatch(
      stateActions.setWebcamInProgress(true));

    try
    {
      await this._updateWebcams();

      const array = Array.from(this._webcams.keys());
      const len = array.length;
      const deviceId =
        this._webcam.device ? this._webcam.device.deviceId : undefined;
      let idx = array.indexOf(deviceId);

      if (idx < len - 1)
        idx++;
      else
        idx = 0;

      this._webcam.device = this._webcams.get(array[idx]);

      logger.debug(
        'changeWebcam() | new selected webcam [device:%o]',
        this._webcam.device);

      // Reset video resolution to HD.
      this._webcam.resolution = 'hd';

      if (!this._webcam.device)
        throw new Error('no webcam devices');

      // Closing the current video track before asking for a new one (mobiles do not like
      // having both front/back cameras open at the same time).
      this._webcamProducer.track.stop();

      logger.debug('changeWebcam() | calling getUserMedia()');

      const stream = await navigator.mediaDevices.getUserMedia(
        {
          video :
            {
              deviceId : { exact: this._webcam.device.deviceId },
              ...VIDEO_CONSTRAINS[this._webcam.resolution]
            }
        });

      const track = stream.getVideoTracks()[0];

      await this._webcamProducer.replaceTrack({ track });

      store.dispatch(
        stateActions.setProducerTrack(this._webcamProducer.id, track));
    }
    catch (error)
    {
      logger.error('changeWebcam() | failed: %o', error);

      store.dispatch(requestActions.notify(
        {
          type : 'error',
          text : `Could not change webcam: ${error}`
        }));
    }

    store.dispatch(
      stateActions.setWebcamInProgress(false));
  }

  async enableShare()
  {
    logger.debug('enableShare()');

    if (this._shareProducer)
      return;
    else if (this._webcamProducer)
      await this.disableWebcam();

    if (!this._mediasoupDevice.canProduce('video'))
    {
      logger.error('enableShare() | cannot produce video');

      return;
    }

    let track;

    store.dispatch(
      stateActions.setShareInProgress(true));

    try
    {
      logger.debug('enableShare() | calling getUserMedia()');

      const stream = await navigator.mediaDevices.getDisplayMedia(
        {
          audio : false,
          video :
            {
              displaySurface : 'monitor',
              logicalSurface : true,
              cursor         : true,
              width          : { max: 1920 },
              height         : { max: 1080 },
              frameRate      : { max: 30 }
            }
        });

      // May mean cancelled (in some implementations).
      if (!stream)
      {
        store.dispatch(
          stateActions.setShareInProgress(true));

        return;
      }

      track = stream.getVideoTracks()[0];

      let encodings;
      let codec;
      const codecOptions =
        {
          videoGoogleStartBitrate : 1000
        };

      if (this._forceVP8)
      {
        codec = this._mediasoupDevice.rtpCapabilities.codecs
          .find((c) => c.mimeType.toLowerCase() === 'video/vp8');

        if (!codec)
        {
          throw new Error('desired VP8 codec+configuration is not supported');
        }
      }
      else if (this._forceH264)
      {
        codec = this._mediasoupDevice.rtpCapabilities.codecs
          .find((c) => c.mimeType.toLowerCase() === 'video/h264');

        if (!codec)
        {
          throw new Error('desired H264 codec+configuration is not supported');
        }
      }
      else if (this._forceVP9)
      {
        codec = this._mediasoupDevice.rtpCapabilities.codecs
          .find((c) => c.mimeType.toLowerCase() === 'video/vp9');

        if (!codec)
        {
          throw new Error('desired VP9 codec+configuration is not supported');
        }
      }

      if (this._enableSharingLayers)
      {
        // If VP9 is the only available video codec then use SVC.
        const firstVideoCodec = this._mediasoupDevice
          .rtpCapabilities
          .codecs
          .find((c) => c.kind === 'video');

        // VP9 with SVC.
        if (
          (this._forceVP9 && codec) ||
          firstVideoCodec.mimeType.toLowerCase() === 'video/vp9'
        )
        {
          encodings =
            [
              {
                maxBitrate      : 5000000,
                scalabilityMode : this._sharingScalabilityMode || 'L3T3',
                dtx             : true
              }
            ];
        }
        // VP8 or H264 with simulcast.
        else
        {
          encodings =
            [
              {
                scaleResolutionDownBy : 1,
                maxBitrate            : 5000000,
                scalabilityMode       : this._sharingScalabilityMode || 'L1T3',
                dtx                   : true
              }
            ];

          if (this._numSimulcastStreams > 1)
          {
            encodings.unshift(
              {
                scaleResolutionDownBy : 2,
                maxBitrate            : 1000000,
                scalabilityMode       : this._sharingScalabilityMode || 'L1T3',
                dtx                   : true
              }
            );
          }

          if (this._numSimulcastStreams > 2)
          {
            encodings.unshift(
              {
                scaleResolutionDownBy : 4,
                maxBitrate            : 500000,
                scalabilityMode       : this._sharingScalabilityMode || 'L1T3',
                dtx                   : true
              }
            );
          }
        }
      }

      this._shareProducer = await this._sendTransport.produce(
        {
          track,
          encodings,
          codecOptions,
          codec,
          appData :
            {
              share : true
            }
        });

      if (this._e2eKey && e2e.isSupported())
      {
        e2e.setupSenderTransform(this._shareProducer.rtpSender);
      }

      store.dispatch(stateActions.addProducer(
        {
          id            : this._shareProducer.id,
          type          : 'share',
          paused        : this._shareProducer.paused,
          track         : this._shareProducer.track,
          rtpParameters : this._shareProducer.rtpParameters,
          codec         : this._shareProducer.rtpParameters.codecs[0].mimeType.split('/')[1]
        }));

      this._shareProducer.on('transportclose', () =>
      {
        this._shareProducer = null;
      });

      this._shareProducer.on('trackended', () =>
      {
        store.dispatch(requestActions.notify(
          {
            type : 'error',
            text : 'Share disconnected!'
          }));

        this.disableShare()
          .catch(() => {});
      });
    }
    catch (error)
    {
      logger.error('enableShare() | failed:%o', error);

      if (error.name !== 'NotAllowedError')
      {
        store.dispatch(requestActions.notify(
          {
            type : 'error',
            text : `Error sharing: ${error}`
          }));
      }

      if (track)
        track.stop();
    }

    store.dispatch(
      stateActions.setShareInProgress(false));
  }

  async disableShare() {
    console.info('disableShare()');

    if (!this._shareProducer) {
      return;
    }

    this._shareProducer.close();

    store.removeProducer(this._shareProducer.id);

    try {
      await this._protoo.request('closeProducer', { producerId: this._shareProducer.id });
    } catch (error) {
      console.error('关闭服务器端共享创建者时出错', error);
    }

    this._shareProducer = null;
  }

  async _joinRoom() {
    console.info('_joinRoom()');

    try {
      this._mediasoupDevice = new mediasoupClient.Device({handlerName: this._handlerName});

      store.setRoomMediasoupClientHandler(this._mediasoupDevice.handlerName);

      const routerRtpCapabilities = await this._protoo.request('getRouterRtpCapabilities');

      await this._mediasoupDevice.load({routerRtpCapabilities});

      // NOTE: Stuff to play remote audios due to browsers' new autoplay policy.Just get access to the mic and DO NOT close the mic track for a while. Super hack!
      {
        const stream = await navigator.mediaDevices.getUserMedia({video: {facingMode: this._facingMode}, audio: true});
        const audioTrack = stream.getAudioTracks()[0];

        audioTrack.enabled = false;

        setTimeout(() => audioTrack.stop(), 120000);
      }
      // Create mediasoup Transport for sending (unless we don't want to produce).
      if (this._produce) {
        const transportInfo = await this._protoo.request('createWebRtcTransport', {
          forceTcp: this._forceTcp,
          producing: true,
          consuming: false,
          sctpCapabilities: this._useDataChannel ? this._mediasoupDevice.sctpCapabilities : undefined
        });

        const {
          id,
          iceParameters,
          iceCandidates,
          dtlsParameters,
          sctpParameters
        } = transportInfo;

        this._sendTransport = this._mediasoupDevice.createSendTransport({
          id,
          iceParameters,
          iceCandidates,
          dtlsParameters: {
            ...dtlsParameters,
            // Remote DTLS role. We know it's always 'auto' by default so, if
            // we want, we can force local WebRTC transport to be 'client' by
            // indicating 'server' here and vice-versa.
            role: 'auto'
          },
          sctpParameters,
          iceServers: [],
          proprietaryConstraints: {},
          additionalSettings: {
            encodedInsertableStreams: this._e2eKey && e2e.isSupported()
          }
        });

        this._sendTransport.on('connect', ({dtlsParameters}, callback, errback) => {
          this._protoo.request('connectWebRtcTransport', {
            transportId: this._sendTransport.id,
            dtlsParameters
          }).then(callback).catch(errback);
        });

        this._sendTransport.on('produce', async ({kind, rtpParameters, appData}, callback, errback) => {
          try {
            const {id} = await this._protoo.request('produce', {
              transportId: this._sendTransport.id,
              kind,
              rtpParameters,
              appData
            });

            callback({id});
          } catch (error) {
            errback(error);
          }
        });

        this._sendTransport.on('producedata', async ({sctpStreamParameters, label, protocol, appData}, callback, errback) => {
          console.debug('"producedata" event: [sctpStreamParameters:%o, appData:%o]', sctpStreamParameters, appData);

          try {
            // eslint-disable-next-line no-shadow
            const {id} = await this._protoo.request('produceData', {
              transportId: this._sendTransport.id,
              sctpStreamParameters,
              label,
              protocol,
              appData
            });

            callback({id});
          } catch (error) {
            errback(error);
          }
        });
      }

      // Create mediasoup Transport for receiving (unless we don't want to consume).
      if (this._consume) {
        const transportInfo = await this._protoo.request('createWebRtcTransport', {
          forceTcp: this._forceTcp,
          producing: false,
          consuming: true,
          sctpCapabilities: this._useDataChannel ? this._mediasoupDevice.sctpCapabilities : undefined
        });

        const {
          id,
          iceParameters,
          iceCandidates,
          dtlsParameters,
          sctpParameters
        } = transportInfo;

        this._recvTransport = this._mediasoupDevice.createRecvTransport({
          id,
          iceParameters,
          iceCandidates,
          dtlsParameters: {
            ...dtlsParameters,
            // Remote DTLS role. We know it's always 'auto' by default so, if
            // we want, we can force local WebRTC transport to be 'client' by
            // indicating 'server' here and vice-versa.
            role: 'auto'
          },
          sctpParameters,
          iceServers: [],
          additionalSettings: {
            encodedInsertableStreams: this._e2eKey && e2e.isSupported()
          }
        });

        this._recvTransport.on('connect', ({dtlsParameters}, callback, errback) => {
          this._protoo.request('connectWebRtcTransport', {
            transportId: this._recvTransport.id,
            dtlsParameters
          }).then(callback).catch(errback);
        });
      }

      // Join now into the room.
      // NOTE: Don't send our RTP capabilities if we don't want to consume.
      const {peers} = await this._protoo.request('join', {
        displayName: this._displayName,
        device: this._device,
        rtpCapabilities: this._consume ? this._mediasoupDevice.rtpCapabilities : undefined,
        sctpCapabilities: this._useDataChannel && this._consume ? this._mediasoupDevice.sctpCapabilities : undefined
      });

      store.setRoomState('connected');

      // Clean all the existing notifcations.
      // store.dispatch(stateActions.removeAllNotifications());

      // store.dispatch(requestActions.notify({
      //     text: 'You are in the room!',
      //     timeout: 3000
      //   }));

      for (const peer of peers) {
        store.addPeer({...peer, consumers: [], dataConsumers: []});
      }

      // Enable mic/webcam.
      if (this._produce) {
        // Set our media capabilities.
        store.setMediaCapabilities({
          canSendMic: this._mediasoupDevice.canProduce('audio'),
          canSendWebcam: this._mediasoupDevice.canProduce('video')
        });

        this.enableMic();

        this.enableWebcam();

        this._sendTransport.on('connectionstatechange', (connectionState) => {
          if (connectionState === 'connected') {
            this.enableChatDataProducer();
            this.enableBotDataProducer();
          }
        });
      }

      // NOTE: For testing.
      if (window.SHOW_INFO) {
        store.setRoomStatsPeerId(store.me.id);
      }
    } catch (error) {
      console.error('_joinRoom() failed:', error);

      this.close();
    }
  }

  /**
   * 更新摄像头
   *
   * @returns {Promise<void>}
   * @private
   */
  async _updateWebcams() {
    console.info('_updateWebcams()');

    // Reset the list.
    this._webcams = new Map();

    console.info('_updateWebcams() | calling enumerateDevices()');

    const devices = await navigator.mediaDevices.enumerateDevices();

    for (const device of devices) {
      if (device.kind !== 'videoinput') {
        continue;
      }

      this._webcams.set(device.deviceId, device);
    }

    const array = Array.from(this._webcams.values());
    const len = array.length;
    const currentWebcamId = this._webcam.device ? this._webcam.device.deviceId : undefined;

    console.info('_updateWebcams() [webcams:%o]', array);

    if (len === 0) {
      this._webcam.device = null;
    } else if (!this._webcams.has(currentWebcamId)) {
      this._webcam.device = array[0];
    }

    store.setCanChangeWebcam(this._webcams.size > 1);
  }

  /**
   * 获取摄像头类型
   *
   * @param device
   * @returns {string}
   * @private
   */
  _getWebcamType(device) {
    if (/(back|rear)/i.test(device.label)) {
      console.info('_getWebcamType() | it seems to be a back camera');

      return 'back';
    } else {
      console.info('_getWebcamType() | it seems to be a front camera');

      return 'front';
    }
  }

  /**
   * 获取外部视频流
   *
   * @returns {Promise<*|null>}
   * @private
   */
  async _getExternalVideoStream() {
    if (this._externalVideoStream) {
      return this._externalVideoStream;
    }

    if (this._externalVideo.readyState < 3) {
      await new Promise((resolve) => {
        this._externalVideo.addEventListener('canplay', resolve)
      });
    }

    if (this._externalVideo.captureStream) {
      this._externalVideoStream = this._externalVideo.captureStream();
    } else if (this._externalVideo.mozCaptureStream) {
      this._externalVideoStream = this._externalVideo.mozCaptureStream();
    } else {
      throw new Error('video.captureStream() not supported');
    }

    return this._externalVideoStream;
  }
}
