import { createRouter, createWebHistory } from 'vue-router';

// 定义中间件
const middleware = {
  guest: (to, from, next) => {
    if (!sessionStorage.getItem('uid') || !sessionStorage.getItem('room') || !sessionStorage.getItem('nickname')) {
      return next();
    } else {
      return next("/");
    }
  },
  auth: (to, from, next) => {
    if (sessionStorage.getItem('uid') && sessionStorage.getItem('room') && sessionStorage.getItem('nickname')) {
      return next();
    } else {
      return next("/login");
    }
  }
}

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      meta: {
        title: '会议室',
      },
      beforeEnter: [middleware.auth],
      component: () => import('@/views/home.vue')
    },
    {
      path: '/login',
      name: 'login',
      meta: {
        title: '登录',
      },
      beforeEnter: [middleware.guest],
      component: () => import('@/views/login.vue')
    }
  ]
});

router.beforeEach((to, from, next) => {
  window.document.title = import.meta.env.APP_NAME + " - " + to.meta.title;
  next();
});

export default router;
