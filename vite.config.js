import { fileURLToPath, URL } from 'node:url';

import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue';
import fs from "fs";
import path from "path";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
  ],
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url))
    }
  },
  envPrefix: ['APP_'],
  publicDir: 'src/public',
  build: {
    // 解决打包兼容性
    target: 'chrome89',
    outDir: 'public',
    // 生产环境移除console
    minify: 'terser',
    terserOptions: {
      compress: {
        drop_console: true,
      },
    },
  },
  server: {
    open: true,
    https: {
      cert: fs.readFileSync(path.join(__dirname, 'ssl/fullchain.pem')),
      key: fs.readFileSync(path.join(__dirname, 'ssl/privkey.pem'))
    }
  }
});
