/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./index.html",
    "./src/**/*.{vue,css,js}",
  ],
  theme: {
    extend: {
      height: {
        '22': '5.5rem',
      },
      minHeight: {
        'main': 'calc(100vh - 5.5rem - 3rem)',
        'main-sm': 'calc(100vh - 3rem - 3rem)',
      },
      backgroundSize: {
        '75': '75%',
        '45': '45%',
        '3.5': '0.875rem',
      }
    },
  },
  plugins: [],
}

